# Transcript of Pepper&Carrot Episode 12 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 12 : Rangement d'Automne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|9|True|Cling
Son|10|True|Clang
Son|11|False|Clong
Cayenne|1|False|Potions fou rire
Cayenne|2|False|Potions méga chevelure
Cayenne|3|False|Potion boules puantes
Cayenne|4|False|Potion la vie en rose
Cayenne|5|False|Potion fumigènes ...
Cayenne|6|False|... et j'en passe !
Pepper|7|True|Oui je sais:
Pepper|8|False|"Une-vraie-sorcière-de-Chaosah ne-doit-pas-faire-ce-genre-de-potions-là".
Cayenne|13|True|Je dois passer au marché de Komona.
Cayenne|14|False|Fais disparaître tout ça en mon absence; ça serait malheureux que de petits rigolos jouent avec ça.
Cayenne|12|False|Oui. Et c'est bien mieux comme ça.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|2|False|Ziooum !|nowhitespace
Pepper|3|False|Flûte!
Pepper|4|True|Tout enterrer ?!
Pepper|5|False|Mais ça va prendre des heures sans même parler des ampoules aux mains!
Son|6|False|Poum
Pepper|1|False|Faire disparaître? Facile! Youpi!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|"Et surtout n'utilise pas la magie. Une vraie sorcière de Chaosah n'utilise pas la magie pour des tâches quotidiennes."
Pepper|2|False|Grrr !
Pepper|3|True|CARROT!
Pepper|4|False|Livre sept de Chaosah: "les Champs Gravitationnels"... ...et tout-de-suite!
Pepper|5|False|Je vais lui montrer ce que c'est qu'une vraie sorcière de Chaosah à cette vieille bique!
Son|6|True|Tam
Son|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|2|False|VRO O oo oo oo oo oo|nowhitespace
Cayenne|3|False|... Un trou noir de Chaosah?!...
Pepper|4|False|?!
Cayenne|5|False|... C'est vraiment ce que tu comprends par: "ne pas utiliser de la magie"?
Pepper|6|False|... Heu... Vous ne deviez pas être au marché?
Cayenne|7|True|Bien sûr que non!
Cayenne|8|False|Et j'ai bien eu raison; c'est impossible de te laisser sans surveillance!
Pepper|1|False|GURGES ATER !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|wo oo wo owo owoo|nowhitespace
Son|7|False|wo oo wo owo owoo|nowhitespace
Son|6|False|Bam
Carrot|10|False|?!
Cayenne|2|False|...Et regarde moi ça.
Cayenne|3|True|Masse insuffisante!
Cayenne|4|True|Faible champ gravitationnel différentiel!
Cayenne|5|False|Même la dernière orbite circulaire stable est trop petite!
Cayenne|9|False|Alors qu'un horizon des évènements un poil plus grand aurait fait l'affaire!
Cayenne|8|False|Et voilà : Presque tout va graviter dans l'orbite stable ou aux points de Lagrange et va continuer à flotter tout autour.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|! ! !|nowhitespace
Cayenne|2|False|! ! !|nowhitespace
Cayenne|3|False|CURSUS ANNULATIONUS MAXIMUS!
Son|4|False|Ch Klak!|nowhitespace
Narrateur|5|False|- FIN -
Crédits|6|False|10/2015 - Dessin & Scénario : David Revoy

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|3|False|https://www.patreon.com/davidrevoy
Crédits|4|False|License : Creative Commons Attribution 4.0 Sources : disponibles sur www.peppercarrot.com Logiciels : cet épisode a été dessiné à 100% avec des logiciels libres Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 on Linux Mint 17
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 575 Mécènes :
Crédits|2|True|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode sur

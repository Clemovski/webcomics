# Transcript of Pepper&Carrot Episode 06 [ns]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Folg 6: De Töverdrunk-Wettstriet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh Schiet, ik heff bi 't Inslapen mal wedder dat Fenster apen laten...
Pepper|2|True|...wat för 'n Wind!
Pepper|3|False|...un worüm seh ik Komona dör dat Fenster?
Pepper|4|False|KOMONA!
Pepper|5|False|De Töverdrunk-Wettstriet!
Pepper|6|True|Ik ... ik bün denn woll ut Versehn wedder inslapen!
Pepper|9|True|... man?
Pepper|10|False|Woneem bün ik?!?
Bird|12|False|K?|nowhitespace
Bird|11|True|qua|nowhitespace
Pepper|7|False|*
Note|8|False|*Kiek Folg 4: En swienplietschen Momang

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Wuddel! Dat is bannig sööt, wat du dor an dacht hest un mi na den Wettstriet bringst!
Pepper|3|False|Al-ler-best!!!
Pepper|4|True|Du hest sogor an en Töverdrunk, mien Kleedaasch un mien Hoot dacht!
Pepper|5|False|Mal kieken, wat vör 'n Töverdrunk dat is...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|WAT?!!
Mayor of Komona|3|False|As Börgermeister vun Komona geev ik nu dat Start-Teken för den Töverdrunk-Wettstriet!
Mayor of Komona|4|False|Uns Stadt freut sik, wat wi för uns eerste Wettstriet nich minner as veer Hexen begröten dörvt.
Mayor of Komona|5|True|Dörv ik nu üm enen
Mayor of Komona|6|True|hartlichen Bifall
Writing|2|False|Töverdrunk-Wettstriet vun Komo
Mayor of Komona|7|False|bidden!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Klapp
Mayor of Komona|1|True|Se kummt vun wiet her ut de grote Technoloogsche Union. Dat is en Ehr, wat se nu hier is, de smucke un plietsche
Mayor of Komona|3|True|Nich to vergeten de Hex ut uns egen Land! Ut Komona, wi hefft
Mayor of Komona|5|True|Uns drüdde Deel-nehmersche kummt ut dat Land, wo de Maanden ünnergaht:
Mayor of Komona|7|True|Un to 'n Sluss uns veerte Deelnehmersche ut 'n Kattekerwoold:
Mayor of Komona|2|False|Koriander!
Mayor of Komona|4|False|Safran!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Laat de Wettstriet nu losgahn!
Mayor of Komona|10|False|Afstimmt warrt dör den Bifall-Meter!
Mayor of Komona|11|False|As eerste warrt nu Koriander ehr Kunst wiesen.
Coriander|12|False|Damen un Herren...
Coriander|13|True|... Se mööt nich mehr bang ween för den Doot vunwegen ...
Coriander|14|True|... mien Drunk, de
Coriander|15|True|ZOMBIFIZEREN
Audience|17|True|Klapp
Audience|18|True|Klapp
Audience|19|True|Klapp
Audience|20|True|Klapp
Audience|21|True|Klapp
Audience|22|True|Klapp
Audience|23|True|Klapp
Audience|24|True|Klapp
Audience|25|True|Klapp
Audience|26|True|Klapp
Audience|27|True|Klapp
Audience|28|True|Klapp
Audience|29|True|Klapp
Coriander|16|False|deit!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|ALLERBEST!
Mayor of Komona|2|False|Koriander hett de Doot överdüvelt mit ehr wunner-boren Töverdrunk!
Audience|4|True|Klapp
Audience|5|True|Klapp
Audience|6|True|Klapp
Audience|7|True|Klapp
Audience|8|True|Klapp
Audience|9|True|Klapp
Audience|10|True|Klapp
Audience|11|True|Klapp
Audience|12|True|Klapp
Audience|13|True|Klapp
Audience|14|True|Klapp
Audience|15|True|Klapp
Audience|16|True|Klapp
Audience|17|False|Klapp
Saffron|19|True|Denn nu kummt
Saffron|18|False|Töövt man noch af mit den Bifall, leve Lüüd vun Komona!
Saffron|22|True|De Töverdrunk, wo Se al lang op luurt hefft: De Drunk, de bannig Indruck op Ehr Navers maken warrt, de...
Saffron|23|False|... se niedsch maken warrt!
Saffron|20|True|MIEN
Saffron|26|False|Ophübschdrunk!
Saffron|24|True|Un dat allens blots dör dat Anwennen vun en lütten Drüppen vun mien...
Audience|27|True|Klapp
Audience|28|True|Klapp
Audience|29|True|Klapp
Audience|30|True|Klapp
Audience|31|True|Klapp
Audience|32|True|Klapp
Audience|33|True|Klapp
Audience|34|True|Klapp
Audience|35|True|Klapp
Audience|36|True|Klapp
Audience|37|True|Klapp
Audience|38|True|Klapp
Audience|39|True|Klapp
Audience|40|False|Klapp
Mayor of Komona|42|False|De Drunk kunn heel Komona riek maken!
Mayor of Komona|41|True|Dull! Nich to glöven!
Audience|44|True|Klapp
Audience|46|True|Klapp
Audience|47|True|Klapp
Audience|48|True|Klapp
Audience|49|True|Klapp
Audience|50|True|Klapp
Audience|51|True|Klapp
Audience|52|True|Klapp
Audience|53|True|Klapp
Audience|54|True|Klapp
Audience|55|True|Klapp
Audience|56|True|Klapp
Audience|57|True|Klapp
Audience|58|True|Klapp
Audience|59|True|Klapp
Audience|60|False|Klapp
Mayor of Komona|43|False|De Bifall is kloor as man wat: Koriander is nu al rut!
Saffron|21|False|Drunk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Dat warrt nich eenfach warrn för Shichimi, dat noch to överbeden.
Shichimi|4|True|NEE!
Shichimi|5|True|Geiht nich, dat is to gefährlich!
Shichimi|6|False|DEIT MI LEED!
Mayor of Komona|3|False|Nu maak to, Shichimi, de hele Welt töövt op di!
Mayor of Komona|7|False|Damen un Herren, dat lett, as wenn Shichimi opgifft.
Saffron|8|False|Lang mi dat her!
Saffron|9|False|Un heff di nich so. Du vermasselst noch de Show.
Saffron|10|False|De Lüüd weet doch so un so al, ik heff wunnen. Is doch puttegal, wat dien Drunk nu noch deit...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z Z ZZ|nowhitespace
Shichimi|15|False|GEWALDIG GROTE MONSTER MAAKT!
Shichimi|2|False|Ik... ik heff datnich weten, wat wi de Drunk hier vör all Lüüd wiesen mööt.
Shichimi|13|True|WOHRSCHAU!!!
Shichimi|14|True|Dat is en Drunk, de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|KRAHKRAHK Raa aa aa ah|nowhitespace
Sound|2|False|WUMM!
Pepper|3|True|Hey, cool!
Pepper|5|False|Mien Drunk schull tominnst för 'n beten wat to Lachen goot ween...
Pepper|4|False|Bün ik also nu an de Reeg?
Mayor of Komona|6|True|Loop, du Dööskopp!
Mayor of Komona|7|False|De Wettstriet is vörbi! Maak, dat du wegkummst!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...so as jümmers haut se all jüst in de Momang af, wo wi an de Reeg sünd.
Pepper|1|True|Sühst du?
Pepper|4|True|Tominnst heff ik nu en Idee, wat wi mit dien "Drunk" maken köönt, Wuddel:
Pepper|5|False|Hier allens wedder in de Reeg bringen un denn torüch na Huus.
Pepper|7|True|Du
Pepper|8|False|veel to grote, inbildsche Zombie-Piepvagel!
Pepper|10|False|Wullt du noch en letzten Drunk utproberen?
Pepper|11|False|Beter nich, wat?
Pepper|6|False|HEY!
Sound|9|False|KR ACH !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jo, lees man nipp un nau, wat dor op steiht...
Pepper|2|False|Scheer di weg ut Komona, oder ik kipp di dat allens över 'n Kopp!
Mayor of Komona|3|True|Ümdat se uns Stadt vör en gresig Malöör reddt hett,
Mayor of Komona|4|False|geevt wi de eerste Platz an Pepper för ehr Drunk, de...??!!
Pepper|7|False|Äh, egentlich is dat gor keen richtigen Töverdrunk. Dat is de Urinproov vun mien Katt vun sien letzten Besöök bi 'n Deertendokter.
Pepper|6|True|Haha! Jo...
Pepper|8|False|...woll beter keen Demonstratschoon, wat?
Narrator|9|False|Folg 6 : De Töverdrunk-Wettstriet
Narrator|10|False|ENN
Writing|5|False|50 000 Ko
Credits|11|False|März 2015 - Grafik un Text : David Revoy, Översetten: Rebecca Breu

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Wuddel is heel un deel free un Open Source un mit Spennen vun Patreon.com finanzeert. Düsse Folg hefft 245 Förderer mööglich maakt:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Du kannst ok mitdoon un de tokamen Folg Stütt geven:
Credits|7|False|Tools: Düsse Folg is mit 100% free Open-Source-Tools maakt worrn. Krita op Linux Mint
Credits|6|False|Open Source: All de Dateien to 'n Bearbeiden un de Schriften gifft dat op de Websteed vun Pepper&Wuddel to 'n Rünnerladen.
Credits|5|False|Lizenz: Creative Commons Attribution Du dörvst afännern, delen, verköpen usw.
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
